import React from "react"

import { Container } from "reactstrap"

import Layout from "../components/layout"
import SEO from "../components/seo"
import About from "../components/about"
import Speaker from "../components/speakers"

import "./index.scss"

const IndexPage = () => (
  <Layout>
    <SEO />
    <Container>
      <About />
      <Speaker />
    </Container>
  </Layout>
)

export default IndexPage
