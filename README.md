1.  **Clone Respostiory**
2.  **Install Dependencies**

    ```sh
    npm install --global gatsby
    yarn
    ```

3.  **Create a Copy of `.env.example` and rename it to `.env`**

4.  **Start Developing**

    ```sh
    gatsby develop
    ```

5) **Deploy**
   ```sh
   gatsby build
   ```
   and then copy contents of `public` directory to your `public_html` on your server

You Can use Netlify to test your site before publish, by running

```sh
yarn deploy
```

create CNAME file in root directory and put your netlify site url to not deploy to new url each time you deploy.

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/gatsbyjs/gatsby-starter-default)

### Add Speakers

Open `src/data/speakers.json` and add your new speaker.
Save speaker images at `src/images/` directory.
