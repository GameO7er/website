const path = require("path")

require("dotenv").config({
  path: `.env`,
})

module.exports = {
  siteMetadata: {
    title: `جشن روز آزادی نرم‌افزار ۱۳۹۸`,
    description: `روز آزادی نرم افزار یا به اختصار (SFD) یک جشن سالیانه جهانی برای نرم افزار‌های آزاد است و از سال ۱۳۸۳ در ایران برگزار می‌شود. سال گذشته مدافعان آزادی نرم افزار این جشن را در ۷۱ نقطه مختلف در سراسر جهان برگزار کردند.`,
    author: "نیما عارفی",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `جشن روز آزادی نرم‌افزار ۱۳۹۸`,
        short_name: `روز آزادی نرم‌افزار`,
        start_url: `/`,
        background_color: `#000`,
        theme_color: `#fff`,
        display: `minimal-ui`,
        icon: `src/images/icon.jpg`, // This path is relative to the root of the site.
      },
    },
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data`,
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        data: '@import "variables.scss"; @import "media-queries.scss";',
        includePaths: ["src/sass"],
      },
    },
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        printRejected: true, // Print removed selectors and processed file names
        develop: true, // Enable while using `gatsby develop`
        // tailwind: true, // Enable tailwindcss support
        whitelist: ["container", "media", "media-right", "media-body"], // Don't remove this selector
        // ignore: ['/ignored.css', 'prismjs/', 'docsearch.js/'], // Ignore files/folders
        // purgeOnly : ['components/', '/main.css', 'bootstrap/'], // Purge only these files/folders
        whitelistPatterns: [/m(t|b|l|r)?-*/, /p(t|b|l|r)?-*/],
      },
		},
		{       
			resolve: '@pasdo501/gatsby-source-woocommerce',
			options: {
			 // Base URL of Wordpress site
				api: process.env.WORDPRESS_API_URL,
				// true if using https. false if nah.
				https: true,
				api_keys: {
					consumer_key: process.env.WORDPRESS_CONSUMER_KEY,
					consumer_secret: process.env.WORDPRESS_CONSUMER_SECRET,
				},
				// Array of strings with fields you'd like to create nodes for...
				fields: ['products'],
				// Version of the woocommerce API to use
				// OPTIONAL: defaults to 'wc/v1'
				api_version: 'wc/v3',
				// OPTIONAL: How many results to retrieve
				per_page: 100
			}
		}
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
